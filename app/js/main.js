'use strict';

var header = $(".main-header");
var menuItem = $(".menu__item");
var dice = $('.icon-dice');
var menuTrigger = $('.js--sidebar');

// Unsplash vars
var ID = '8385bc6ccc36cbdf38886f4400e096a3b7ee70514e08a8c304968dce78b0aa5b';
var photoAPI = 'https://api.unsplash.com/photos/random?featured&client_id=' + ID;
var currentPage = 1;
var cover = $('.intro__cover');
var introBG = $('.intro');
var random = $('.randomize');
var author = $('.js--unsplash-author');
var link = $('.js--unsplash-link');

var newsName = $('.js--news-name');
var newsBio = $('.js--news-bio');
var newsLocation = $('.js--news-location');
var newsDate = $('.js--news-date');
var newsImage = $('.js--news-image');
var downloadHref = $('.js--download');

// RTL vars
var switcher = $('.dir-switcher');
var icon = $('.dir-switcher .icon');
var switcherText = $('.js-text');

// Load more vars
var loadMore = $('.js--loadMore');
var spinner = $('.icon-spinner');


$(document).ready(function () {

    jQuery(window).trigger("resize");

    init_sectionFull();
    init_clickRandom();
    init_clickMore();
    init_clickRTL();
    init_SmoothScroll();
    init_menuActive();
    RandomUnsplash();
    MoreUnsplash();
    mobileSidebar();

    $(window).on("resize", function() {
        if ($(window).width() > 992) {
            init_sectionFull();
        }

    });

    // Document events
    $(window).scroll(function () {
        var scroll = $(window).scrollTop();
        if (scroll >= 30) {
            header.removeClass('top').addClass("sticky");
        } else {
            header.removeClass("sticky").addClass('top');
        }
    });


});

function init_sectionFull() {
    var viewportHeight = jQuery(window).height();
    jQuery(".intro").css({'min-height': viewportHeight});
}

function init_menuActive() {
    $(menuItem).on("click", function () {
        $(menuItem).removeClass("active");
        $(this).addClass("active");

    })
}

function init_clickRTL() {
    switcher.on('click', function () {
        if (document.querySelector('html').getAttribute('dir') == 'rtl') {
            document.querySelector('html').setAttribute('dir', 'ltr');
            icon.removeClass('pressed');
            switcherText.html("LTR");

        } else {
            document.querySelector('html').setAttribute('dir', 'rtl');
            icon.addClass('pressed');
            switcherText.html("RTL");
        }
    });
}

function init_clickRandom() {
    random.on('click', function () {
        $(dice).addClass("icon--spin");
        RandomUnsplash();
    });
}

function RandomUnsplash() {
    cover.fadeIn();

    $.getJSON(photoAPI)
        .done(function (data) {
            console.log(data)
            $.get(data.urls.full, function (info) {
            }).done(function () {
                cover.fadeOut("slow");
                $(dice).removeClass("icon--spin");
            }).fail(function (info) {
                // alert("Unsplash is not responsive or request limit is reached.")
            });

            // INTRO: bg, user, user href
            $(introBG).css('background-image', 'url(' + data.urls.full + ')');
            $(author).html(data.user.name);
            $(link).attr("href", data.user.links.html + "?utm_source=xsolve-task&utm_medium=referral");

            // ARTICLE: bg, date, user, location, download
            var imageSrc = data.urls.full;
            $(newsDate).html(moment(data.created_at).endOf('day').fromNow())
            $(newsImage).css("background-image", 'url(' + imageSrc + ')');

            $(newsName).html(data.user.name);
            $(newsBio).html(data.user.bio);
            $(newsLocation).html(data.location.title);

            $(downloadHref).attr("href", data.links.download);


        })
        .fail(function (data) {
            alert("Unsplash is not responsive or request limit is reached.");
            $(dice).removeClass("icon--spin");
        });
}

function init_clickMore() {
    loadMore.on('click', function (e) {
        e.preventDefault();
        MoreUnsplash();
    });
}

function MoreUnsplash() {
    var htmlText = '';
    var randomAPI = 'https://api.unsplash.com/photos?page=' + currentPage + '&per_page=3&count=3&client_id=' + ID;

    currentPage += 1;
    spinner.fadeIn();

    $.getJSON(randomAPI)
        .done(function (data) {

            for (var key in data) {
                var day = data[key].created_at;
                htmlText += '<div class="cell-flex large-4">';
                htmlText += '<a class="" href="' + data[key].links.html + '" target="_blank">';
                htmlText += '<article class="news-small" style="background-image:url(' + data[key].urls.small + ')">';
                htmlText += '<span class="news-small__info"><svg class="icon icon-like"><use xlink:href="#icon-like"></use></svg><bdi> ' + data[key].likes + '&nbsp;</bdi></span>';
                htmlText += '<span class="news-small__info"><svg class="icon icon-clock"><use xlink:href="#icon-clock"></use></svg><bdi> ' + moment(day).endOf('day').fromNow() + '&nbsp;</bdi></span>';
                htmlText += '<h3 class="news-small__title">' + data[key].user.name + '</h3>';
                htmlText += '<p class="news-small__excerpt"></p>';
                htmlText += '</article>';
                htmlText += '</a>';
                htmlText += '</div>';
            }

            // $('.js--news-small').append(htmlText).hide().slideDown("slow");
            $(htmlText).appendTo('.js--news-small').hide().slideDown("slow");
            htmlText = '';
            spinner.fadeOut();

        })
        .fail(function (data) {
            alert("Unsplash is not responsive or request limit is reached.")
            spinner.fadeOut();
        });
}

function init_SmoothScroll() {
    jQuery('a[href*="#"]:not([href="#"])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = jQuery(this.hash);
            target = target.length ? target : jQuery('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                jQuery('html, body').animate({
                    scrollTop: target.offset().top - 600
                }, 1000);
                return false;
            }
        }
    });
}

function mobileSidebar() {
    menuTrigger.on('click', function (e) {
        e.preventDefault();
        $('.sidebar').toggleClass("sidebar--opened");
        $('.sidebar--trigger .icon').toggleClass("active");
    });
}