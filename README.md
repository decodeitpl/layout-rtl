# RTL/LTR Website for XSolve

Recruitment task for XSolve Frontend Developer.

Live version of Website [DEMO](https://www.decodeit.pl/xsolve/)

### Getting Started


#### Familiar with Git?
Checkout this repo, install dependencies, then start the bundle process with the following:

```
> git clone git@bitbucket.org:decodeitpl/layout-rtl.git
> cd xsolve-task
> npm install
> gulp build
```

Start development server:
`gulp`
